"use strict";

// Часть №1
{
    class Pokemon {
        constructor(name, level) {
            this.name = name;
            this.level = level;
        }
        show() {
            console.log('Имя: ' + this.name + ', уровень: ' + this.level);
        };
    }

    class PokemonList {
        constructor(name, arr) {
            this.name = name;
            this.arr = arr;
        }
        add(pokemon) {
            this.arr.push(pokemon);
        };
        show() {
            console.log('Всего: ' + this.arr.length + ' покемонов');
            this.arr.forEach(i => i.show());
        };
        move(toGroup, pokemonName) {
            let pokemonIndex = this.arr.findIndex((x) => {return x.name === pokemonName});
            let movedPokemon = this.arr.splice(pokemonIndex, 1);
            toGroup.arr.push(movedPokemon[0]);
        };
        //max() {
        //    let max = 0, maxIndex;
        //    for(var i = 0; i < this.arr.length; i ++) {
        //        if(this.arr[i].level > max) {
        //            max = this.arr[i].level;
        //            maxIndex = i;
        //        }
        //    }
        //    return console.log(this.arr[maxIndex]);
        //};
    }

// Часть №2
    let pokemon1 = new Pokemon('Godzilla1', 30);
    let pokemon2 = new Pokemon('Godzilla2', 60);
    let pokemon3 = new Pokemon('Godzilla3', 90);
    let pokemon4 = new Pokemon('King-Kong1', 40);
    let pokemon5 = new Pokemon('King-Kong2', 70);
    let pokemon6 = new Pokemon('King-Kong3', 100);

    let lost = new PokemonList('lost', []);
    let found = new PokemonList('found', []);
    lost.add(pokemon1);
    lost.add(pokemon2);
    lost.add(pokemon3);
    found.add(pokemon4);
    found.add(pokemon5);
    found.add(pokemon6);

    lost.move(found, 'Godzilla2');
    //lost.show();
    //found.show();
    //found.max();
}